package com.onlineshoppingportal.first.app.services;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlineshoppingportal.first.app.entities.Category;
import com.onlineshoppingportal.first.app.DTO.CategoryDTO;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.DuplicateException;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.RecordNotFoundException;
import com.onlineshoppingportal.first.app.repositories.CategoryRepository;

@Service
public class CategoryService {
	@Autowired
	CategoryRepository categoryRepository;
	
//	@Transactional
	public void createCategory(Category category) {
		categoryRepository.findByName(category.getName()).ifPresent(
			existingCategory -> {
				throw new DuplicateException("Category has already existed!");
			}
		);
		
		if(category.getParentCategory() != null) {
			Category parentCategory = category.getParentCategory();
			
			categoryRepository.findByName(parentCategory.getName())
							  .orElseThrow(() -> new RecordNotFoundException("Parent category does not exist"));
		}
		
		categoryRepository.saveAndFlush(category);
	}
	
	@Transactional
	public CategoryDTO findCategoryByName(String name) throws RecordNotFoundException{
		Category category = categoryRepository.findByName(name).orElseThrow(
				() -> new RecordNotFoundException("Parent category does not exist")
		);
		
		CategoryDTO categoryDTO = new CategoryDTO();
		categoryDTO.setDescription(category.getName());
		categoryDTO.setImageUrl(category.getImageUrl());
		categoryDTO.setName(category.getName());
		return categoryDTO;
	}
	
	@Transactional
	public void deleteCategoryByName(String name) throws RecordNotFoundException {
		Category category = categoryRepository.findByName(name).orElseThrow(
				() -> new RecordNotFoundException("No customer is associated with this id"));
		
		categoryRepository.deleteById(category.getCategoryId());
	}
	
	@Transactional
	public Set<Category> findSubcategories(String name) {
		Category category = categoryRepository.findByName(name).orElseThrow(
				() -> new RecordNotFoundException("There is no category with this name")
		);
		
		return category.getSubCategories();
	}
	
	@Transactional
	public void updateCategory(Category category) {
		Category existedCategory = categoryRepository.findByName(category.getName()).orElseThrow(
				() -> new RecordNotFoundException("There is no category with this name")
		);
		
		if(category.getParentCategory() != null) {
			Category parentCategory = category.getParentCategory();
			categoryRepository.findByName(parentCategory.getName())
				 .orElseThrow(() -> new RecordNotFoundException("There is no parent category with this name"));
		}
				
		existedCategory.setName(category.getName());
		existedCategory.setDescription(category.getDescription());
		existedCategory.setImageUrl(category.getImageUrl());
		existedCategory.setParentCategory(category.getParentCategory());

		categoryRepository.save(existedCategory);
	}
}
