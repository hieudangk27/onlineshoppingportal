package com.onlineshoppingportal.first.app.services;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlineshoppingportal.first.app.DTO.ProductDTO;
import com.onlineshoppingportal.first.app.entities.Category;
import com.onlineshoppingportal.first.app.entities.Product;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.DuplicateException;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.RecordNotFoundException;
import com.onlineshoppingportal.first.app.repositories.CategoryRepository;
import com.onlineshoppingportal.first.app.repositories.ProductRepository;

@Service
public class ProductService {
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Transactional
	public void createProduct(ProductDTO productDTO) {
		if(productRepository.findByName(productDTO.getName()).isEmpty()) {			
			Product newProduct = new Product();
			BeanUtils.copyProperties(productDTO, newProduct);
			
			productRepository.save(newProduct);
		}else {
			throw new DuplicateException("Duplicate name");
		}
	}
	
	@Transactional
	public Product findProductByName(String name) throws RecordNotFoundException{
		Product product = productRepository.findByName(name).orElseThrow(
				() -> new RecordNotFoundException("No product is associated with this name"));
		
		return product;
	}
	
	@Transactional
	public void deleteByName(String name) throws RecordNotFoundException {
		Product product = productRepository.findByName(name).orElseThrow(
				() -> new RecordNotFoundException("No product is associated with this name"));
		
		productRepository.deleteById(product.getProductId());
	}
	
	@Transactional
	public void updateProduct(Product product) {
		Product existedProduct = productRepository.findByName(product.getName()).orElseThrow(
				() -> new RecordNotFoundException("No product is associated with this name")
		);

		existedProduct.setDescription(product.getDescription());
		existedProduct.setNumberOfUnitsAvailable(product.getNumberOfUnitsAvailable());
		existedProduct.setNumberOfUnitsOrdered(product.getNumberOfUnitsOrdered());;
		existedProduct.setPricePerUnit(product.getPricePerUnit());
		existedProduct.setQuantity(product.getQuantity());
		
		productRepository.save(existedProduct);
	}
	
	public void addProductToCategory(String productName, String categoryName) {
		Product product = productRepository.findByName(productName).orElseThrow(
				() -> new RecordNotFoundException("No product is associated with this name")
		);
        Category category = categoryRepository.findByName(categoryName).orElseThrow(
        		() -> new RecordNotFoundException("No category is associated with this name"));
        
        product.getCategories().add(category);
        category.getProducts().add(product);
        productRepository.save(product);
        categoryRepository.save(category);
    }

    public void removeProductFromCategory(String productName, String categoryName) {
    	Product product = productRepository.findByName(productName).orElseThrow(
				() -> new RecordNotFoundException("No product is associated with this name")
		);
        Category category = categoryRepository.findByName(categoryName).orElseThrow(
        		() -> new RecordNotFoundException("No category is associated with this name"));
        
        product.getCategories().remove(category);
        category.getProducts().remove(product);
        productRepository.save(product);
        categoryRepository.save(category);
    }
    
    @Transactional
	public Set<Category> findCategories(String name) {
		Product product = productRepository.findByName(name).orElseThrow(
				() -> new RecordNotFoundException("No product is associated with this name")
		);
		
		return product.getCategories();
	}
}
