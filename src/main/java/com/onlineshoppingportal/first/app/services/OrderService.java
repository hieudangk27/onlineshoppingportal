package com.onlineshoppingportal.first.app.services;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.onlineshoppingportal.first.app.DTO.OrderDTO;
import com.onlineshoppingportal.first.app.DTO.ProductDTO;
import com.onlineshoppingportal.first.app.entities.Customer;
import com.onlineshoppingportal.first.app.entities.Order;
import com.onlineshoppingportal.first.app.entities.Product;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.DuplicateException;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.RecordNotFoundException;
import com.onlineshoppingportal.first.app.repositories.CategoryRepository;
import com.onlineshoppingportal.first.app.repositories.CustomerRepository;
import com.onlineshoppingportal.first.app.repositories.OrderRepository;
import com.onlineshoppingportal.first.app.repositories.ProductRepository;

@Service
public class OrderService {	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Transactional
	public void createOrder(OrderDTO orderDTO) {
		if(orderRepository.findById(orderDTO.getOrderId()).isEmpty()) {			
			Order newOrder = new Order();
			BeanUtils.copyProperties(orderDTO, newOrder);
			
			orderRepository.save(newOrder);
		}else {
			throw new DuplicateException("Duplicate name");
		}
	}
	
	@Transactional
	public Order findOrderById(long id) throws RecordNotFoundException{
		Order order = orderRepository.findById(id).orElseThrow(
				() -> new RecordNotFoundException("No order is associated with this name"));
		
		return order;
	}
	
	@Transactional
	public void deleteOrderById(long id) throws RecordNotFoundException {
		Order order = orderRepository.findById(id).orElseThrow(
				() -> new RecordNotFoundException("No order is associated with this name"));
		
		orderRepository.deleteById(order.getOrderId());
	}
	
	@Transactional
	public void updateOrder(Order order) {
		Order existedOrder = orderRepository.findById(order.getOrderId()).orElseThrow(
				() -> new RecordNotFoundException("No order is associated with this name")
		);
		
		Customer existedCustomer = customerRepository.findById(order.getCustomer().getId())
				.orElseThrow(
						() -> new RecordNotFoundException("The customer does not exist")
				);

		existedOrder.setAddress(order.getAddress());
		existedOrder.setCity(order.getCity());
		existedOrder.setCountry(order.getCountry());
		existedOrder.setCustomer(existedCustomer);
		existedOrder.setOrderDate(order.getOrderDate());
		existedOrder.setPostalCode(order.getPostalCode());
		existedOrder.setShippedDate(order.getShippedDate());
		
		orderRepository.save(existedOrder);
	}
}
