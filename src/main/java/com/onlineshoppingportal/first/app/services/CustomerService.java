package com.onlineshoppingportal.first.app.services;

import javax.transaction.Transactional;

import java.util.Set;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlineshoppingportal.first.app.entities.Customer;
import com.onlineshoppingportal.first.app.entities.Order;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.DuplicateException;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.RecordNotFoundException;
import com.onlineshoppingportal.first.app.repositories.CustomerRepository;
import com.onlineshoppingportal.first.app.DTO.CustomerDTO;

@Service
public class CustomerService {
	@Autowired
	private CustomerRepository customerRepository;
	
	@Transactional
	public void createCustomer(CustomerDTO customerDTO) {
		if(customerRepository.findByEmail(customerDTO.getEmail()).isEmpty() 
				&& customerRepository.findByPhoneNumber(customerDTO.getPhoneNumber()).isEmpty()){
			
			Customer newCustomer = new Customer();
			BeanUtils.copyProperties(customerDTO, newCustomer);
			
			customerRepository.save(newCustomer);
		}else {
			throw new DuplicateException("Duplicate email or phone number");
		}
	}
	
	@Transactional
	public Customer findCustomerById(long id) throws RecordNotFoundException{
		Customer customer = customerRepository.findById(id).orElseThrow(
				() -> new RecordNotFoundException("No customer is associated with this id"));
		
		return customer;
	}
	
	@Transactional
	public void deleteCustomerById(long id) throws RecordNotFoundException {
		customerRepository.findById(id).orElseThrow(
				() -> new RecordNotFoundException("No customer is associated with this id"));
		
		customerRepository.deleteById(id);
	}
	
	@Transactional
	public void updateCustomer(Customer customer) {
		Customer existedCustomer = customerRepository.findByEmail(customer.getEmail()).orElseThrow(
				() -> new RecordNotFoundException("No customer is associated with this email")
		);
		
		customerRepository.findByPhoneNumber(customer.getPhoneNumber()).ifPresent(
			existingCustomer ->{
				if (!existingCustomer.getEmail().equals(customer.getEmail())) {
	                throw new DuplicateException("This phone number is currently being used");
				}
			}
		);
			
		existedCustomer.setFirstName(customer.getFirstName());
		existedCustomer.setLastName(customer.getLastName());
		existedCustomer.setCity(customer.getCity());
		existedCustomer.setCountry(customer.getCountry());
		existedCustomer.setPostalCode(customer.getPostalCode());
		existedCustomer.setPhoneNumber(customer.getPhoneNumber());
		existedCustomer.setAddress(customer.getAddress());
		
		customerRepository.save(existedCustomer);
	}
	
	@Transactional
	public Set<Order> findOrders(long id) throws RecordNotFoundException{
		Customer customer = customerRepository.findById(id).orElseThrow(
				() -> new RecordNotFoundException("No customer is associated with this id"));
		
		return customer.getOrders();
	}
}
