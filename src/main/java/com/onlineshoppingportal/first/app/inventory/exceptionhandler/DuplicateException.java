package com.onlineshoppingportal.first.app.inventory.exceptionhandler;

public class DuplicateException extends RuntimeException {
	String message;
	
	public DuplicateException(String message) {
		super(message);
	}
}
