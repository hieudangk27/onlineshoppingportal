package com.onlineshoppingportal.first.app.inventory.exceptionhandler;

public class RecordNotFoundException extends RuntimeException {
	String message;
	
	public RecordNotFoundException(String message) {
		super(message);
	}
}
