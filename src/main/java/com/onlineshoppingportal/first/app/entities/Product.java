package com.onlineshoppingportal.first.app.entities;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id")
	private long productId;
	
	@Column(nullable=false)
	private String name;
	
	@Column(nullable=false)
	private long pricePerUnit;
	
	@Column(nullable=false)
	private int numberOfUnitsAvailable;
	
	@Column(nullable=false)
	private int numberOfUnitsOrdered;
	
	@Column(nullable=false)
	private int quantity;
		
	private String description;
	
	public Product(long productId, String name, int pricePerUnit, int numberOfUnitsAvailable, int numberOfUnitsOrdered,
			int quantity) {
		super();
		this.productId = productId;
		this.name = name;
		this.pricePerUnit = pricePerUnit;
		this.numberOfUnitsAvailable = numberOfUnitsAvailable;
		this.numberOfUnitsOrdered = numberOfUnitsOrdered;
		this.quantity = quantity;
	}
	
	public Product() {
		
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPricePerUnit() {
		return pricePerUnit;
	}

	public void setPricePerUnit(long pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	public int getNumberOfUnitsAvailable() {
		return numberOfUnitsAvailable;
	}

	public void setNumberOfUnitsAvailable(int numberOfUnitsAvailable) {
		this.numberOfUnitsAvailable = numberOfUnitsAvailable;
	}

	public int getNumberOfUnitsOrdered() {
		return numberOfUnitsOrdered;
	}

	public void setNumberOfUnitsOrdered(int numberOfUnitsOrdered) {
		this.numberOfUnitsOrdered = numberOfUnitsOrdered;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToMany(mappedBy = "products")
	private Set<Category> categories;	
}
