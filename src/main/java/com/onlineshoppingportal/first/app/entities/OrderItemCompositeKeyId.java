package com.onlineshoppingportal.first.app.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderItemCompositeKeyId implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "order_id")
	private long orderId;
	
	@Column(name = "product_id")
	private long productId;
	
	public OrderItemCompositeKeyId(long orderId, long productId) {
		super();
		this.orderId = orderId;
		this.productId = productId;
	}
	
	public OrderItemCompositeKeyId() {
		
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}
	
//	@Override
//	public boolean equals(Object obj) {
//		return super.equals(obj);
//	}
//	
//	@Override
//	public int hashCode() {
//		return Objects.hash(orderId, productId);
//	}
}
