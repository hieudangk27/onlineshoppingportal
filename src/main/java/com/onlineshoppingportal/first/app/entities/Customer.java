package com.onlineshoppingportal.first.app.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "customers")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customer_id")
	private long id;
	@Column(name = "first_name", columnDefinition = "nvarchar(80)", nullable = false)
	private String firstName;
	@Column(name = "last_name", columnDefinition = "nvarchar(80)", nullable = false)
	private String lastName;
	@Column(name = "email", columnDefinition = "nvarchar(50)", nullable = false, unique=true)
	private String email;
	@Column(columnDefinition = "nvarchar(60)")
	private String address;
	@Column(columnDefinition = "nvarchar(30)")
	private String city;
	@Column(columnDefinition = "nvarchar(50)")
	private String country;
	private int postalCode;
	@Column(name = "phone_number", columnDefinition = "nvarchar(50)", unique=true)
	private String phoneNumber;
	
	public Customer(long id, String firstName, String lastName, String address, String city, String country,
			int postalCode, String phoneNumber) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.city = city;
		this.country = country;
		this.postalCode = postalCode;
		this.phoneNumber = phoneNumber;
	}
	
	public Customer() {
		
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public int getPostalCode() {
		return postalCode;
	}
	
	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	// 1 customer có nhiều orders
	@OneToMany(mappedBy = "customer")
	private Set<Order> orders;
	
	public Set<Order> getOrders(){
		return this.orders;
	}
}
