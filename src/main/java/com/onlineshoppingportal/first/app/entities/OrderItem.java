package com.onlineshoppingportal.first.app.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class OrderItem {
	@EmbeddedId
	private OrderItemCompositeKeyId id;
	
	@Column(nullable = false)
	private long pricePerUnit;
	
	@Column(nullable = false)
	private int quantity;
	
	@Column(nullable = false)
	private int discount;

	public OrderItem(OrderItemCompositeKeyId id, long pricePerUnit, int quantity, int discount) {
		super();
		this.id = id;
		this.pricePerUnit = pricePerUnit;
		this.quantity = quantity;
		this.discount = discount;
	}
	
	public OrderItem() {
		
	}

	public OrderItemCompositeKeyId getId() {
		return id;
	}

	public void setId(OrderItemCompositeKeyId id) {
		this.id = id;
	}

	public long getPricePerUnit() {
		return pricePerUnit;
	}

	public void setPricePerUnit(long pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	@MapsId("order_id")
    @JoinColumn(name = "order_id")
    @ManyToOne
    private Order order;
	
	@MapsId("product_id")
    @JoinColumn(name = "product_id")
    @ManyToOne
    private Product product;
}
