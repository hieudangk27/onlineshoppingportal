package com.onlineshoppingportal.first.app.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "categories")
public class Category {
	private static final long ID = UUID.randomUUID().getMostSignificantBits();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_id")
	private long categoryId;
	
	@Column(nullable=false, unique = true, columnDefinition = "nvarchar(60)")
	private String name;
	
	@Column(columnDefinition = "text")
	private String description;
	
	@Column(columnDefinition = "text")
	private String imageUrl;

	public Category(long categoryId, String name, String description, String imageUrl) {
		super();
		this.categoryId = categoryId;
		this.name = name;
		this.description = description;
		this.imageUrl = imageUrl;
	}
	
	public Category() {
		
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@ManyToMany
	@JoinTable(name = "category_product",
	joinColumns = @JoinColumn(name = "category_id", referencedColumnName = "category_id"),
	inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "product_id"))
	private Set<Product> products;

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	} 
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_category_id")
    private Category parentCategory;

    @OneToMany(mappedBy = "parentCategory", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Category> subCategories;
    
    public long getParentCategoryId() {
    	return this.parentCategory.getCategoryId();
    }
    
    public Category getParentCategory() {
    	return parentCategory;
    }
    
    public void setParentCategory(Category parentCategory) {
    	this.parentCategory = parentCategory;
    }
    
    public Set<Category> getSubCategories(){
    	return subCategories;
    }
    
}
