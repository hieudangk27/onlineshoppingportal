package com.onlineshoppingportal.first.app.DTO;

public class OrderItemDTO {
	private int customerId;
	private int orderId;
	private long pricePerUnit;
	private int quantity;
	private int discount;

	public OrderItemDTO(int customerId, int orderId, long pricePerUnit, int quantity, int discount) {
		super();
		this.customerId = customerId;
		this.orderId = orderId;
		this.pricePerUnit = pricePerUnit;
		this.quantity = quantity;
		this.discount = discount;
	}
	
	public OrderItemDTO() {
		
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public long getPricePerUnit() {
		return pricePerUnit;
	}

	public void setPricePerUnit(long pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}
}
