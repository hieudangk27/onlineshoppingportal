package com.onlineshoppingportal.first.app.DTO;

public class ProductDTO {
	private String name;
	private long pricePerUnit;
	private int numberOfUnitsAvailable;
	private int numberOfUnitsOrdered;
	private int quantity;
	private int categoryID;
	private String description;
	
	public ProductDTO(String name, long pricePerUnit, int numberOfUnitsAvailable, int numberOfUnitsOrdered,
			int quantity, int categoryID, String description) {
		super();
		this.name = name;
		this.pricePerUnit = pricePerUnit;
		this.numberOfUnitsAvailable = numberOfUnitsAvailable;
		this.numberOfUnitsOrdered = numberOfUnitsOrdered;
		this.quantity = quantity;
		this.categoryID = categoryID;
		this.description = description;
	}
	
	public ProductDTO() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPricePerUnit() {
		return pricePerUnit;
	}

	public void setPricePerUnit(long pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	public int getNumberOfUnitsAvailable() {
		return numberOfUnitsAvailable;
	}

	public void setNumberOfUnitsAvailable(int numberOfUnitsAvailable) {
		this.numberOfUnitsAvailable = numberOfUnitsAvailable;
	}

	public int getNumberOfUnitsOrdered() {
		return numberOfUnitsOrdered;
	}

	public void setNumberOfUnitsOrdered(int numberOfUnitsOrdered) {
		this.numberOfUnitsOrdered = numberOfUnitsOrdered;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
