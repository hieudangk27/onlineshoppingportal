package com.onlineshoppingportal.first.app.DTO;

public class CategoryDTO {
	private long categoryId;
	private String name;
	private String description;
	private String imageUrl;
	public CategoryDTO(long categoryId, String name, String description, String imageUrl) {
		super();
		this.categoryId = categoryId;
		this.name = name;
		this.description = description;
		this.imageUrl = imageUrl;
	}
	
	public CategoryDTO(){
		
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
