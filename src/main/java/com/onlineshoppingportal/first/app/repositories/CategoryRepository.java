package com.onlineshoppingportal.first.app.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.onlineshoppingportal.first.app.entities.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{
	Optional<Category> findByName(String name);
	Optional<Category> findByParentCategory(Category category);
}
