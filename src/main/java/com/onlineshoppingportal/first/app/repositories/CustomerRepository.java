package com.onlineshoppingportal.first.app.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onlineshoppingportal.first.app.entities.Customer;
import com.onlineshoppingportal.first.app.DTO.CustomerDTO;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	Optional<Customer> findByEmail(String email);
	Optional<Customer> findByPhoneNumber(String phoneNumber);
}
