package com.onlineshoppingportal.first.app.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.onlineshoppingportal.first.app.entities.Product;
import com.onlineshoppingportal.first.app.entities.Category;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.RecordNotFoundException;
import com.onlineshoppingportal.first.app.services.ProductService;
import com.onlineshoppingportal.first.app.DTO.ProductDTO;

@Controller
public class ProductController {
	@Autowired
	ProductService productService;
	
	@PostMapping("/product")
	public ResponseEntity<?> createCategory(@RequestBody ProductDTO productDTO) {
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			productService.createProduct(productDTO);
			response = new ResponseEntity<>(headers, HttpStatus.CREATED);
			return response;
		} catch (Exception e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.CONFLICT);
			return response;
		}
	}
	
	@GetMapping(value="/product")
	public ResponseEntity<?> findCategoryByName(@RequestParam String name){
		System.out.println("yooyoyoy " + name);
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			Product product = productService.findProductByName(name);
			response = new ResponseEntity<>(product, headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@GetMapping(value="/categories")
	public ResponseEntity<?> findCategories(@RequestParam String name){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			Set<Category> set = productService.findCategories(name);
			response = new ResponseEntity<>(set, headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@DeleteMapping(value="/product")
	public ResponseEntity<?> deleteCustomerById(@RequestParam String name){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			productService.deleteByName(name);
			response = new ResponseEntity<>(headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@PutMapping("/product")
	public ResponseEntity<?> updateCustomer(@RequestBody Product product) {
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			productService.updateProduct(product);
			response = new ResponseEntity<>(headers, HttpStatus.CREATED);
			return response;
		} catch (Exception e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.CONFLICT);
			return response;
		}
	}
	
	@PostMapping("/product_category")
	public ResponseEntity<?> addProductToCategory(@RequestParam String productName,
			@RequestParam String categoryName){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			productService.addProductToCategory(productName, categoryName);
			response = new ResponseEntity<>(headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@DeleteMapping("/product_category")
	public ResponseEntity<?> removeProductFromCategory(@RequestParam String productName,
			@RequestParam String categoryName){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			productService.removeProductFromCategory(productName, categoryName);
			response = new ResponseEntity<>(headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
}
