package com.onlineshoppingportal.first.app.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.onlineshoppingportal.first.app.entities.Product;
import com.onlineshoppingportal.first.app.entities.Category;
import com.onlineshoppingportal.first.app.entities.Order;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.RecordNotFoundException;
import com.onlineshoppingportal.first.app.services.OrderService;
import com.onlineshoppingportal.first.app.services.ProductService;
import com.onlineshoppingportal.first.app.DTO.OrderDTO;
import com.onlineshoppingportal.first.app.DTO.ProductDTO;

@Controller
public class OrderController {
	@Autowired
	OrderService orderService;
	
	@PostMapping("/order")
	public ResponseEntity<?> createCategory(@RequestBody OrderDTO orderDTO) {
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			orderService.createOrder(orderDTO);
			response = new ResponseEntity<>(headers, HttpStatus.CREATED);
			return response;
		} catch (Exception e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.CONFLICT);
			return response;
		}
	}
	
	@GetMapping(value="/order")
	public ResponseEntity<?> findCategoryByName(@RequestParam long id){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			Order order = orderService.findOrderById(id);
			response = new ResponseEntity<>(order, headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@DeleteMapping(value="/order")
	public ResponseEntity<?> deleteCustomerById(@RequestParam long id){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			orderService.deleteOrderById(id);
			response = new ResponseEntity<>(headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@PutMapping("/product")
	public ResponseEntity<?> updateCustomer(@RequestBody Order order) {
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			orderService.updateOrder(order);
			response = new ResponseEntity<>(headers, HttpStatus.CREATED);
			return response;
		} catch (Exception e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.CONFLICT);
			return response;
		}
	}
}
