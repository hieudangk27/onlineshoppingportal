package com.onlineshoppingportal.first.app.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.onlineshoppingportal.first.app.entities.Category;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.RecordNotFoundException;
import com.onlineshoppingportal.first.app.services.CategoryService;
import com.onlineshoppingportal.first.app.DTO.CategoryDTO;

@Controller
public class CategoryController {
	@Autowired
	CategoryService categoryService;
	
	@PostMapping("/category")
	public ResponseEntity<?> createCategory(@RequestBody Category category) {
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			categoryService.createCategory(category);
			response = new ResponseEntity<>(headers, HttpStatus.CREATED);
			return response;
		} catch (Exception e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.CONFLICT);
			return response;
		}
	}
	
	@GetMapping(value="/category")
	public ResponseEntity<?> findCategoryByName(@RequestParam String name){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			CategoryDTO categoryDTO = categoryService.findCategoryByName(name);
			response = new ResponseEntity<>(categoryDTO, headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@GetMapping(value="/subcategories")
	public ResponseEntity<?> findSubcategories(@RequestParam String name){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			Set<Category> set = categoryService.findSubcategories(name);
			response = new ResponseEntity<>(set, headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@DeleteMapping(value="/category")
	public ResponseEntity<?> deleteCustomerById(@RequestParam String name){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			categoryService.deleteCategoryByName(name);
			response = new ResponseEntity<>(headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@PutMapping("/category")
	public ResponseEntity<?> updateCustomer(@RequestBody Category category) {
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			categoryService.updateCategory(category);
			response = new ResponseEntity<>(headers, HttpStatus.CREATED);
			return response;
		} catch (Exception e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.CONFLICT);
			return response;
		}
	}
	
	
	
	
}
