package com.onlineshoppingportal.first.app.controllers;

import java.awt.PageAttributes.MediaType;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpHeaders;

import com.onlineshoppingportal.first.app.entities.Order;
import com.onlineshoppingportal.first.app.entities.Customer;
import com.onlineshoppingportal.first.app.DTO.CustomerDTO;
import com.onlineshoppingportal.first.app.inventory.exceptionhandler.RecordNotFoundException;
import com.onlineshoppingportal.first.app.services.CustomerService;

@RestController
public class CustomerController {
	@Autowired
	private CustomerService customerService;
	
	@PostMapping("/customer")
	public ResponseEntity<?> createCustomer(@RequestBody CustomerDTO customerDTO) {
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			customerService.createCustomer(customerDTO);
			response = new ResponseEntity<>(
					headers, HttpStatus.CREATED);
			return response;
		} catch (Exception e) {
			response = new ResponseEntity<>(e.getMessage(),
					headers, HttpStatus.CONFLICT);
			return response;
		}
	}
	
	@GetMapping(value="/customer")
	public ResponseEntity<?> findCustomerById(@RequestParam long id){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			Customer customer = customerService.findCustomerById(id);
			response = new ResponseEntity<>(customer, headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@DeleteMapping(value="/customer")
	public ResponseEntity<?> deleteCustomerById(@RequestParam long id){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			customerService.deleteCustomerById(id);
			response = new ResponseEntity<>(headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
	
	@PutMapping("/customer")
	public ResponseEntity<?> updateCustomer(@RequestBody Customer customer) {
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			customerService.updateCustomer(customer);
			response = new ResponseEntity<>(
					headers, HttpStatus.CREATED);
			return response;
		} catch (Exception e) {
			response = new ResponseEntity<>(e.getMessage(),
					headers, HttpStatus.CONFLICT);
			return response;
		}
	}
	
	@GetMapping(value="/customer/orders")
	public ResponseEntity<?> findAllOrdersByCustomer(@RequestParam long id){
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<?> response;
		try {
			Set<Order> orders = customerService.findOrders(id);
			response = new ResponseEntity<>(orders, headers, HttpStatus.ACCEPTED);
			return response;
		} catch (RecordNotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), headers, HttpStatus.NOT_FOUND);
			return response;
		}
	}
}
