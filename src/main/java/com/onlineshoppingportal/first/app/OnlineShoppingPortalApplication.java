package com.onlineshoppingportal.first.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineShoppingPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineShoppingPortalApplication.class, args);
	}

}
