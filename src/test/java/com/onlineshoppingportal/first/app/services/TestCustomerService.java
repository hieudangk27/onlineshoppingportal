//package com.onlineshoppingportal.first.app.services;
//
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Set;
//
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import com.onlineshoppingportal.first.app.entities.Customer;
//import com.onlineshoppingportal.first.app.entities.Category;
//import com.onlineshoppingportal.first.app.entities.Product;
//import com.onlineshoppingportal.first.app.entities.Order;
//import com.onlineshoppingportal.first.app.entities.OrderItem;
//import com.onlineshoppingportal.first.app.entities.OrderItemCompositeKeyId;
//import com.onlineshoppingportal.first.app.repositories.CategoryRepository;
//import com.onlineshoppingportal.first.app.repositories.CustomerRepository;
//import com.onlineshoppingportal.first.app.repositories.OrderRepository;
//import com.onlineshoppingportal.first.app.repositories.OrderItemRepository;
//import com.onlineshoppingportal.first.app.repositories.ProductRepository;
//
//@SpringBootTest
//class TestCustomerService {
//	@Autowired
//	private CustomerRepository customerRepository;
//	
//	@Autowired
//	private CategoryRepository categoryRepository;
//	
//	@Autowired
//	private OrderRepository orderRepository;
//	
//	@Autowired
//	private OrderItemRepository orderItemRepository;
//	
//	@Autowired
//	private ProductRepository productRepository;
//	
//	@Test
//	void test() {
//		Customer newCustomer1 = new Customer();
//		newCustomer1.setFirstName("Hoa");
//		newCustomer1.setLastName("Bui");
//		newCustomer1.setEmail("buihoa@gmail.com");
//		newCustomer1.setAddress("Cau Giay");
//		newCustomer1.setCity("Hanoi");
//		newCustomer1.setCountry("Vietnam");
//		newCustomer1.setPhoneNumber("84-12345");
//		newCustomer1.setPostalCode(100000);
//
//		customerRepository.save(newCustomer1);
//		
//		Product product1 = new Product();
//		product1.setName("Nồi áp suất");
//		product1.setNumberOfUnitsAvailable(5);
//		product1.setNumberOfUnitsOrdered(2);
//		product1.setPricePerUnit(500000);
//		product1.setQuantity(2);
//		product1.setCategoryID(1);
//		
//		Product product2 = new Product();
//		product2.setName("Nồi cơm điện");
//		product2.setNumberOfUnitsAvailable(8);
//		product2.setNumberOfUnitsOrdered(4);
//		product2.setPricePerUnit(1000000);
//		product2.setQuantity(3);
//		product2.setCategoryID(1);
//		
//		Product product3 = new Product();
//		product3.setName("Máy rửa bát");
//		product3.setNumberOfUnitsAvailable(10);
//		product3.setNumberOfUnitsOrdered(10);
//		product3.setPricePerUnit(6000000);
//		product3.setQuantity(1);
//		product3.setCategoryID(2);
//
//		productRepository.save(product1);
//		productRepository.save(product2);
//		productRepository.save(product3);
//		
//		Category category1 = new Category();
//		category1.setName("Nội thất gia đình");
//		category1.setDescription("Các sản phẩm nội thất");
//		category1.setImageUrl("test1.jpg");
//		
//		Category category2 = new Category();
//		category2.setName("Category 2");
//		category2.setDescription("Category thứ 2");
//		category2.setImageUrl("test2.jpg");
//		
//		categoryRepository.save(category1);
//		categoryRepository.save(category2);
//		
//		// Tìm tất cả products thuộc category có ID = 1
//		Set<Product> setProducts = categoryRepository.findById(1L).get().getProducts();
//		// Tìm tất cả categories mà một product có ID = 1 thuộc về
//		Set<Category> setCategories = productRepository.findById(1L).get().getCategories();
//	
//		Order order1 = new Order();
//		order1.setCustomer(newCustomer1);
//		order1.setCity("Hanoi");
//		order1.setCountry("Vietnam");
//		order1.setOrderDate(new Date(8));
//		order1.setShippedDate(new Date(9));
//		order1.setPostalCode(100000);
//		order1.setAddress("Ba Dinh");
//		
//		// order2
//		Order order2 = new Order();
//		order2.setCustomer(newCustomer1);
//		order2.setCity("Hanoi");
//		order2.setCountry("Vietnam");
//		order2.setOrderDate(new Date(7));
//		order2.setShippedDate(new Date(8));
//		order2.setPostalCode(100000);
//		order2.setAddress("Dong Da");
//		
//		orderRepository.save(order1);
//		orderRepository.save(order2);
//		
//		// orderitem1
//		OrderItem orderItem1 = new OrderItem();
//		orderItem1.setId(new OrderItemCompositeKeyId(1, 1));
//		orderItem1.setQuantity(5);
//		orderItem1.setPricePerUnit(50000);
//		orderItem1.setDiscount(10);
//		
//		OrderItem orderItem2 = new OrderItem();
//		orderItem2.setId(new OrderItemCompositeKeyId(1, 2));
//		orderItem2.setQuantity(5);
//		orderItem2.setPricePerUnit(50000);
//		orderItem2.setDiscount(10);
//		
//		orderItemRepository.save(orderItem1);
//		orderItemRepository.save(orderItem2);
//	}
//}
